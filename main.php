<?php
//scandir = liste le contenu d'un dossier
$files = scandir("./Models");
foreach($files as $file){
    $path = './Models/'.$file;
    //is_file = indique si le fichier est un véritable fichier
    //si c'est le cas, importe son contenu
    if(is_file($path)) include_once($path);
}
$domDuTonnerre = array(
    new Orc("BolloDeFresa",new Warrior()),
    new Elf("Loic",new Drood()),
);
//postulat de départ, $i = true
while($i=1){
    $i = 0; //false ; $i = 0 correspond au premier indice du tableau $domDuTonnerre
    $domDuTonnerre[$i]->attack($domDuTonnerre[!$i]); //humain maia attaque elf loic
    sleep(1.08);
    if($domDuTonnerre[!$i]->health <= 0){ //si la vie de elf loic inférieur ou égal à 0
        echo $domDuTonnerre[!$i]->name . " is dead \n"; // elf loic mort, maia gagne
        break;
    } 

    $domDuTonnerre[!$i]->attack($domDuTonnerre[$i]); //inverse donc : elf loic --> humain maia
    if($domDuTonnerre[$i]->health <= 0){ //si la vie de humain maia inférieur ou égal à 0
        echo $domDuTonnerre[$i]->name . " is dead \n"; //elf loic gagne
        break;
    } 
}

/**
 * Le dome du tonerre v1 :
 * 
 * Deux HOMMES rentrent, un HOMME sort
 * 
 * Mettre deux guerriers dans une liste nommée le DOM_DU_TONNERRE
 * 
 * Faire en sorte que :
 * - les deux combattants tapent à tour de rôle jusqu'à ce que l'un d'entre eux meure. 1st commit
 * - les combattants attaquent de manière aléatoire jusqu'à ce que l'un d'eux meure. 2nd commit
 * 
 * TIPS : 
 *      - Les personnages possèdent une méthode attaquer qui prend en argument la cible de l'attaque.
 *      - Le calcul des dégats est un truc du genre PV_DEF -= FOR_ATT - ENDU_DEF * 0.25 + time()² % 3
 *      - Afficher en console un message du style : 
 *          ATT_NAME a attaqué DEF_NAME et lui a infligé xx pts de dégats.
 *          DEF_NAME se sent a l'article de wordpress, il lui reste xxx PV.
 */