<?
/**
 * Sauf pour l'endurance les valeurs maximales des autres attributs sont 50
 * + les modificateurs des races 
 * + et des rôles
 * 
 * Ajustez la valeur de l'endurance pour avoir des combats pas trop long ni trop court.
 * 
 * A la création d'un personnage, les valeurs des attributs principaux sont tirés aléatoirement
 * avec comme valeur totale max 50 (ou comprise entre 30 et 50).
 */
class Character {
    //propriété de l'objet de type Character
    public $force;
    public $agility;
    public $endurance;
    public $health;
    public $role;
    public $name;
    //construction de l'objet
    public function __construct($name,$role){
        $this->name = $name;
        $this->generateValues();
        $this->health = 1000;
        $this->role = $role;
        //ajustement selon les rôles
        $this->force += $this->role->force;
        $this->agility += $this->role->agility;
        $this->endurance += $this->role->endurance;
    }

    /**
     * Methode :
     * génère une valeur aléatoire entre 0 et 50 pour chaque attributs
     * la méthode est private parce qu'elle est appelée uniquement dans le constructeur
     */
    private function generateValues(){
        $ok = false;
        do{
            $this->force = rand(0,50);
            $this->agility = rand(0,50);
            $this->endurance = rand(0,50);
            $total = $this->force + $this->agility + $this->endurance;
            if($total >= 30 && $total <= 50) $ok = true;
        }while($ok);
        
    }    
    //Méthode générique
    public function attack($cible){
        echo $this->name . " attacked " . $cible->name . "\n inflicted ";
        $degats = $this->force - $cible->endurance;
        $cible->health -= $degats;
        echo $degats . " points of damage \n" . $cible->name . " has " . $cible->health . " points left of PV. \n";
    }
        
}