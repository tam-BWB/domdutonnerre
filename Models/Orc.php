<?
//si jamais le fichier est déjà inclus, évite qu'il ne le soit 2 fois
include_once('Character.php');
/**
 * un humain possède 25% de pv en plus
 */
class Orc extends Character {
    /**
     * une fois que le constructeur du parent est éxécuté, pour chaque new Human 
     * on ajoute 25% à leur points de vie
     */
    public function __construct($name,$role){
        //appel au constructeur parent pour éxcuter le code du constructeur
        parent::__construct($name,$role); 
        $this->force *= 1.25;  // OU $this->health = intval($this->health * 1.25);
    }
}