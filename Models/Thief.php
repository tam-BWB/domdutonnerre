<?
/**
 * Les classes des Types/Rôles possèdent des valeurs aux attributs à modifier.
 */
class Thief {
    public $force;
    public $agility;
    public $endurance;
    public function __construct(){
        $this->force = 3;
        $this->agility = 25;
        $this->endurance = 10;
    }
}