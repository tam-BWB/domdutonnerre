<?
/**
 * Les classes des Types/Rôles possèdent des valeurs aux attributs à modifier.
 */
class Archer {
    public $force;
    public $agility;
    public $endurance;
    public function __construct(){
        $this->force = 3;
        $this->agility = 10;
        $this->endurance = 5;
    }
}