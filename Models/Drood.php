<?
/**
 * Les classes des Types/Rôles possèdent des valeurs aux attributs à modifier.
 */
class Drood {
    public $force;
    public $agility;
    public $endurance;
    public function __construct(){
        $this->force = 1;
        $this->agility = 10;
        $this->endurance = 0;
    }
}